package blatstvo.calculator.View;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import blatstvo.calculator.R;
import blatstvo.calculator.View.display.AdvancedDisplay;



public class DisplayOverlay extends FrameLayout {
    private static final float MIN_SETTLE_DURATION = 200f;

    /**
     * Do not settle overlay if velocity is less than this
     */
    private static float VELOCITY_SLOP = 0.1f;

    private static boolean DEBUG = false;
    private static final String TAG = "DisplayOverlay";

    private RecyclerView mRecyclerView;
    private View mFormula;
    private View mResult;
    private LinearLayoutManager mLayoutManager;
    private float mInitialMotionY;
    private float mLastMotionY;
    private float mLastDeltaY;
    private int mTouchSlop;
    private int mMaxTranslationInParent = -1;
    private VelocityTracker mVelocityTracker;
    private float mMinVelocity = -1;
    private int mParentHeight = -1;

    /**
     * Reports when state changes to expanded or collapsed (partial is ignored)
     */
    public static interface TranslateStateListener {
        public void onTranslateStateChanged(TranslateState newState);
    }

    private TranslateStateListener mTranslateStateListener;

    public DisplayOverlay(Context context) {
        super(context);
        setup();
    }

    public DisplayOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public DisplayOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    public DisplayOverlay(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup();
    }

    private void setup() {
        ViewConfiguration vc = ViewConfiguration.get(getContext());
        mTouchSlop = vc.getScaledTouchSlop();
    }

    public static enum TranslateState {
        EXPANDED, COLLAPSED, PARTIAL
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mRecyclerView = (RecyclerView)findViewById(R.id.historyRecycler);
        mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mFormula = findViewById(R.id.formula);
        mResult = findViewById(R.id.result);
        //mMainDisplay = findViewById(R.id.mainDisplay);
//        mCloseGraphHandle = findViewById(R.id.closeGraphHandle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        int action = MotionEventCompat.getActionMasked(ev);
        float y = ev.getRawY();
        TranslateState state = getTranslateState();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mInitialMotionY = y;
                mLastMotionY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                float dy = y - mInitialMotionY;
                if (Math.abs(dy) < mTouchSlop) {
                    return false;
                }

                if (dy < 0) {
                    return isScrolledToEnd() && state != TranslateState.COLLAPSED;
                } else if (dy > 0) {
                    return state != TranslateState.EXPANDED;
                }

                break;
        }

        return false;
    }

    private boolean isScrolledToEnd() {
        return mLayoutManager.findLastCompletelyVisibleItemPosition() ==
                mRecyclerView.getAdapter().getItemCount() - 1;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
        initVelocityTrackerIfNotExists();
        mVelocityTracker.addMovement(event);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                handleMove(event);
                break;
            case MotionEvent.ACTION_UP:
                handleUp(event);
                recycleVelocityTracker();
                break;
            case MotionEvent.ACTION_CANCEL:
                recycleVelocityTracker();
                break;
        }

        return true;
    }

    private void handleMove(MotionEvent event) {
        TranslateState state = getTranslateState();
        float y = event.getRawY();
        float dy = y - mLastMotionY;
        if (DEBUG) {
            Log.v(TAG, "handleMove y=" + y + ", dy=" + dy);
        }

        if (dy < 0 && state != TranslateState.COLLAPSED) {
            updateTranslation(dy);
        } else if (dy > 0 && state != TranslateState.EXPANDED) {
            updateTranslation(dy);
        }
        mLastMotionY = y;
        mLastDeltaY = dy;
    }

    private void handleUp(MotionEvent event) {
        mVelocityTracker.computeCurrentVelocity(1);
        float yvel = mVelocityTracker.getYVelocity();
        if (DEBUG) {
            Log.v(TAG, "handleUp yvel=" + yvel + ", mLastDeltaY=" + mLastDeltaY);
        }

        TranslateState curState = getTranslateState();
        if (curState != TranslateState.PARTIAL) {
            // already settled
            if (mTranslateStateListener != null) {
                mTranslateStateListener.onTranslateStateChanged(curState);
            }
        } else if (Math.abs(yvel) > VELOCITY_SLOP) {
            // the sign on velocity seems unreliable, so use last delta to determine direction
            float destTx = mLastDeltaY > 0 ? getMaxTranslation() : 0;
            float velocity = Math.max(Math.abs(yvel), Math.abs(mMinVelocity));
            settleAt(destTx, velocity);
        }
    }

    public void expandHistory() {
        settleAt(getMaxTranslation(), mMinVelocity);
    }

    public void collapseHistory() {
        settleAt(0, mMinVelocity);
    }

    public int getDisplayHeight() {
        return mFormula.getHeight() + mResult.getHeight();
    }

    /**
     * Smoothly translates the display overlay to the given target
     *
     * @param destTx target translation
     * @param yvel   velocity at point of release
     */
    private void settleAt(float destTx, float yvel) {
        if (yvel != 0) {
            float dist = destTx - getTranslationY();
            float dt = Math.abs(dist / yvel);
            if (DEBUG) {
                Log.v(TAG, "settle display overlay yvel=" + yvel +
                        ", dt = " + dt);
            }

            ObjectAnimator anim =
                    ObjectAnimator.ofFloat(this, "translationY",
                            getTranslationY(), destTx);
            anim.setDuration((long) dt);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mTranslateStateListener != null) {
                        mTranslateStateListener.onTranslateStateChanged(getTranslateState());
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            anim.start();
        }
    }

    /**
     * The distance that we are able to pull down the display to reveal history.
     */
    private int getMaxTranslation() {
        if (mMaxTranslationInParent < 0) {
            int bottomPadding = getContext().getResources()
                    .getDimensionPixelOffset(R.dimen.history_view_bottom_margin);
            mMaxTranslationInParent = getParentHeight() - getDisplayHeight();// - bottomPadding;
            if (DEBUG) {
                Log.v(TAG, "mMaxTranslationInParent = " + mMaxTranslationInParent);
            }
        }
        return mMaxTranslationInParent;
    }

    private void updateTranslation(float dy) {
        float txY = getTranslationY() + dy;
        float clampedY = Math.min(Math.max(txY, 0), getMaxTranslation());
        setTranslationY(clampedY);
    }

    private TranslateState getTranslateState() {
        float txY = getTranslationY();
        if (txY <= 0) {
            return TranslateState.COLLAPSED;
        } else if (txY >= getMaxTranslation()) {
            return TranslateState.EXPANDED;
        } else {
            return TranslateState.PARTIAL;
        }
    }

    public RecyclerView getHistoryView() {
        return  mRecyclerView;
    }

    private void initVelocityTrackerIfNotExists() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void recycleVelocityTracker() {
        if (mVelocityTracker != null) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }
    }

    private int getParentHeight() {
        if (mParentHeight < 0) {
            ViewGroup parent = (ViewGroup) getParent();
            mParentHeight = parent.getHeight();
        }
        return mParentHeight;
    }

    public void initializeHistoryAndGraphView() {
        int maxTx = getMaxTranslation();

        if (mRecyclerView.getLayoutParams().height <= 0) {
            MarginLayoutParams historyParams = (MarginLayoutParams)mRecyclerView.getLayoutParams();
            historyParams.height = maxTx;
            MarginLayoutParams overlayParams =
                    (MarginLayoutParams)getLayoutParams();
            overlayParams.topMargin = -maxTx;
            requestLayout();
            scrollToMostRecent();
        }

        if (mMinVelocity < 0) {
            int txDist = getMaxTranslation();
            mMinVelocity = txDist / MIN_SETTLE_DURATION;
        }
    }

    public void scrollToMostRecent() {
        mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
    }

    public void setTranslateStateListener(TranslateStateListener listener) {
        mTranslateStateListener = listener;
    }

    public TranslateStateListener getTranslateStateListener() {
        return mTranslateStateListener;
    }


}
