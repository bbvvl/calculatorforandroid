package blatstvo.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import com.xlythe.math.Base;
import com.xlythe.math.History;
import com.xlythe.math.Persist;

import blatstvo.calculator.View.DisplayOverlay;
import blatstvo.calculator.View.display.AdvancedDisplay;
import blatstvo.calculator.View.viewpager.CalculatorPadViewPager;

public class Calculator extends Activity implements CalculatorExpressionEvaluator.EvaluateCallback, View.OnLongClickListener {

    private static final String NAME = Calculator.class.getName();
    private static final String KEY_CURRENT_STATE = NAME + "_currentState";
    private static final String KEY_CURRENT_EXPRESSION = NAME + "_currentExpression";

    public static final int INVALID_RES_ID = -1;

    private enum CalculatorState {
        INPUT, EVALUATE, RESULT, ERROR
    }

    private CalculatorState mCurrentState;
    private CalculatorPadViewPager mPadViewPager;
    private CalculatorExpressionEvaluator mEvaluator;
    private AdvancedDisplay mFormulaEditText;
    private AdvancedDisplay mResultEditText;

    private DisplayOverlay mDisplayView;
    private History mHistory;
    private RecyclerView.Adapter mHistoryAdapter;
    private Persist mPersist;
    private final TextWatcher mFormulaTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            setState(CalculatorState.INPUT);
            mEvaluator.evaluate(editable, Calculator.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mDisplayView = (DisplayOverlay) findViewById(R.id.display);


        mFormulaEditText = (AdvancedDisplay) findViewById(R.id.formula);
        mResultEditText = (AdvancedDisplay) findViewById(R.id.result);
        mFormulaEditText.addTextChangedListener(mFormulaTextWatcher);
        mEvaluator = new CalculatorExpressionEvaluator();
        mFormulaEditText.setSolver(mEvaluator.getSolver());
        mResultEditText.setSolver(mEvaluator.getSolver());

        mResultEditText.setEnabled(false);
        // Disable IME for this application
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        findViewById(R.id.op_clr).setOnLongClickListener(this);

       /* Button dot = (Button) findViewById(R.id.op_dot);
        dot.setText(String.valueOf(Constants.DECIMAL_POINT));
*/
        restoreValues(savedInstanceState);
        mDisplayView.bringToFront();

        //mFormulaEditText.setcol
        mFormulaEditText.setTextColor(
                getResources().getColor(R.color.display_formula_text_color));
        mResultEditText.setTextColor(
                getResources().getColor(R.color.display_result_text_color));


        //mDisplayView.setMode(displayMode);
        mDisplayView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            if (mDisplayView.getHeight() > 0) {
                mDisplayView.initializeHistoryAndGraphView();
                       /* if (mDisplayView.getMode() == DisplayMode.GRAPH) {
                            mGraphController.startGraph(mFormulaEditText.getText());
                        }*/
            }
        });

        // if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
        mPadViewPager = (CalculatorPadViewPager) findViewById(R.id.view_pager);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Load new history
        mPersist = new Persist(this);
        mPersist.load();
        mHistory = mPersist.getHistory();

        mHistoryAdapter = new HistoryAdapter(this, mHistory,
                entry -> {
                    mFormulaEditText.insert(entry.getResult());
                    mDisplayView.collapseHistory();
                });
        mHistory.setObserver((History.Observer) mHistoryAdapter);
        mDisplayView.getHistoryView().setAdapter(mHistoryAdapter);
        mDisplayView.scrollToMostRecent();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPersist.save();
    }

    private void restoreValues(Bundle savedInstanceState) {

        savedInstanceState = savedInstanceState == null ? Bundle.EMPTY : savedInstanceState;
        setBase(Base.DECIMAL);
        setState(CalculatorState.values()[
                savedInstanceState.getInt(KEY_CURRENT_STATE, CalculatorState.INPUT.ordinal())]);

        mFormulaEditText.setText(
                savedInstanceState.getString(KEY_CURRENT_EXPRESSION, ""));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putInt(KEY_CURRENT_STATE, mCurrentState.ordinal());
        outState.putString(KEY_CURRENT_EXPRESSION, mFormulaEditText.getText());
    }

    @Override
    public void onBackPressed() {
        //if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
        if (mPadViewPager == null || mPadViewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first pad (or the pad is not paged),
            // allow the system to handle the Back button.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous pad.
            mPadViewPager.setCurrentItem(mPadViewPager.getCurrentItem() - 1);
        }
        // }
    }

    public void onButtonClick(View v) {

        switch (v.getId()) {
            case R.id.op_equal:
                onEquals();
                break;
            case R.id.op_clr:
                onDelete();
                break;
            case R.id.fun_sin:
            case R.id.fun_cos:
            case R.id.fun_tg:
            case R.id.fun_ln:
            case R.id.fun_log:

            case R.id.fun_atg:
            case R.id.fun_asin:
            case R.id.fun_acos:
                mFormulaEditText.insert(((Button) v).getText() + "(");
                break;
            default:
                // Clear the input if we are currently displaying a result, and if the key pressed
                // is not a postfix or infix operator.
                CharSequence buttonText = ((Button) v).getText();

  /*              String buttonString = buttonText.toString();
                if (mCurrentState == CalculatorState.RESULT && !(buttonString.equals(getString(R.string.op_division_button_text)) ||
                        buttonString.equals(getString(R.string.op_multiplication_button_text)) ||
                        buttonString.equals(getString(R.string.op_subtraction_button_text)) ||
                        buttonString.equals(getString(R.string.op_addition_button_text)) ||
                        buttonString.equals(getString(R.string.op_pow_button_text)) ||
                        buttonString.equals(getString(R.string.op_factorial_button_text)) ||
                        buttonString.equals(getString(R.string.op_equal_button_text))
                )) {
                    mFormulaEditText.clear();
                }
*/
                mFormulaEditText.insert(buttonText);
                break;
        }
    }

    private void setBase(Base base) {

        // Update the evaluator, which handles the math
        mEvaluator.setBase(mFormulaEditText.getText(), base, (expr, result, errorResourceId) -> {
            if (errorResourceId != INVALID_RES_ID) {
                onError(errorResourceId);
            } else {
                //mResultEditText.setText(result);
                if (!TextUtils.isEmpty(result)) {
                    onResult(result);
                }
            }
        });

    }

    private void onEquals() {

        if (mCurrentState == CalculatorState.INPUT) {
            if (mFormulaEditText.hasNext()) {
                mFormulaEditText.next();
            } else {
                setState(CalculatorState.EVALUATE);
                mEvaluator.evaluate(mFormulaEditText.getText(), this);
            }
        }

    }

    @Override
    public void onEvaluate(String expr, String result, int errorResourceId) {


        if (mCurrentState == CalculatorState.INPUT) {

            if (result == null || result.equals(mFormulaEditText.getText())) {
                mResultEditText.clear();
            } else {
                mResultEditText.setText(result);
            }
        } else if (errorResourceId != INVALID_RES_ID) {
            onError(errorResourceId);
        } else if (!TextUtils.isEmpty(result)) {
            mHistory.enter(expr, result);
            mDisplayView.scrollToMostRecent();
            onResult(result);
        } else if (mCurrentState == CalculatorState.EVALUATE) {
            // The current expression cannot be evaluated -> return to the input state.
            setState(CalculatorState.INPUT);
        }
    }

    private void setState(CalculatorState state) {
        if (mCurrentState != state) {
            mCurrentState = state;
        }
    }

    private void onResult(final String result) {

        mFormulaEditText.setText(result);
        setState(CalculatorState.RESULT);

    }

    private void onError(final int errorResourceId) {
        if (mCurrentState != CalculatorState.EVALUATE) {
            mResultEditText.setText(errorResourceId);
            return;
        }
        setState(CalculatorState.ERROR);
        mResultEditText.setText(errorResourceId);
    }

    @Override
    public boolean onLongClick(View view) {

        if (view.getId() == R.id.op_clr) {
            onClear();
            return true;
        }
        return false;
    }

    private void onClear() {
        if (TextUtils.isEmpty(mFormulaEditText.getText())) {
            return;
        }
        mFormulaEditText.clear();
    }

    private void onDelete() {
        // Delete works like backspace; remove the last character from the expression.
        mFormulaEditText.backspace();

    }

}
